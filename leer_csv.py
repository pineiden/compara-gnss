from csv import DictReader
from pathlib import Path
import sys
from dateutil import parser
from typing import List, Tuple
from datetime import datetime

def read_csv(pathname, ext='csv', fieldnames:list=[])->List[Tuple[datetime, dict]]:
    dataset = []
    path = Path(pathname)
    if path.exists():
        with open(path, 'r') as fileread:
            reader =  DictReader(
                fileread,
                delimiter=',',
                fieldnames=fieldnames if fieldnames else None )
            if fieldnames:
                next(reader)
            for row in reader:
                dtvalue = parser.parse(row["datetime"].replace("_"," ")+"+00:00")
                dataset.append((dtvalue, row))
    return dataset

def list_to_dict(dataset, main_key='datetime'):
    diccionario = {parser.parse(e.get(main_key,0)):e for e in dataset}
    assert len(diccionario.keys())==len(dataset), "No tienen la misma cantidad elementos"
    return diccionario


if __name__=="__main__":
    pathname = sys.argv[1]
    print("Directorio a analizar", pathname)
    dataset = leer_csv(pathname)
    diccionario = list_to_dict(dataset)
    print(len(diccionario.keys()))
