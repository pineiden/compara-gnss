import sys
import click
from pathlib import Path
from pprint import pprint
from typing import List
#// read json path
from leer_json  import read_json
#// read csv
from leer_csv import read_csv, list_to_dict
from functools import partial
from multiprocessing import Pool
import typer
from typing import List, Tuple
from datetime import datetime
import matplotlib.pyplot as plt
from dtypes import Dataset, DataKeys
from rich.table import Table
from rich.console import Console
import gc

app = typer.Typer()

def read_file(filepath:str) -> List[str]:
    lines=[]
    with open(filepath) as f:
        for l in f.readlines():
            lines.append(l.replace("\n",''))
    return lines



def ls(path:str, value, action=print, proc=6):
    data = []
    with Pool(processes=proc) as pool:
        results = pool.map(action, Path(path).glob(value))
        for r in results:
            if r:
                data += r
    return data


def keys_only_bnc(keys_bnc:DataKeys, keys_panga:DataKeys)->DataKeys:
    """
    Extraer solo los puntos que estén en datos bnc, retornando la lista
    de llaves, en datetime
    """
    return keys_bnc-keys_panga

def data_no_panga(data_bnc:dict, keys_unicas:DataKeys)->dict:
    return {key: data_bnc.get(key) for key in keys_unicas}

def coord(panga: dict, v: datetime, n=0) -> float:
    return panga.get(v,{}).get("features")[0]["geometry"]["coordinates"][n]

def procesar_data(station:str, bnc:Dataset, panga:Dataset, tabla:Table, console:Console):
    data_bnc = dict(bnc)
    data_panga = dict(panga)
    keys_bnc = data_bnc.keys()
    keys_panga = data_panga.keys()
    console.log(f"Sorting {station}")
    fechas_ordenadas = sorted(keys_bnc)
    fechas_panga = sorted(keys_panga)
    kob = keys_only_bnc(keys_bnc, keys_panga)
    data_only_bnc = data_no_panga(data_bnc, kob)
    set_no_panga = set(sorted(data_only_bnc.keys()))
    console.log(f"Binary data for {station} no panga")
    all_data_binary =  [(v, 1 if v in set_no_panga else None) for v in
                        fechas_ordenadas]
    from_csv = [(v, float(data_bnc.get(v).get("E"))) for v in
                fechas_ordenadas]
    from_panga = [(v, coord(data_panga, v)) for v in fechas_panga]
    first_date = ""
    last_date = ""
    if fechas_ordenadas:
        first_date = fechas_ordenadas[0].isoformat()
        last_date = fechas_ordenadas[-1].isoformat()
    console.log(f"Fechas {first_date} to {last_date}")
    coincidencia = None
    diferencia = None
    if data_bnc:
        coincidencia =  len(data_panga)/len(data_bnc)
        diferencia = 1-coincidencia
    valores = [station, first_date, last_date, str(coincidencia), str(diferencia)]
    console.log(f"Valores {station} -> {valores}")
    tabla.add_row(*valores)
    del data_bnc
    del data_panga
    del bnc
    del panga
    del from_panga
    del from_csv
    del all_data_binary
    gc.collect()

def graficar(from_csv, from_panga):
    pprint(from_csv[:10])
    pprint(from_panga[:10])
    fig, axs = plt.subplots(2,1)
    l1,l2=axs[0].plot(*zip(*from_csv),*zip(*from_panga))
    axs[0].legend((l1,l2),("PPP","PANGA"),loc="upper right",shadow=True)
    axs[0].set_xlabel("time")
    axs[0].set_ylabel("E[m]")
    axs[1].scatter(*zip(*all_data_binary))
    axs[1].set_xlabel("time")
    axs[1].set_ylabel("1 = no está en panga")
    axs[0].grid(True)
    axs[1].grid(True)
    fig.tight_layout()
    plt.savefig("cons_ppp_panga.svg")
    plt.show()

def crear_tabla()->Table:
    table = Table(show_header=True, header_style="bold magenta")
    table.add_column("Estación", justify="left")
    table.add_column("Fecha Inicio", style="dim", width=12)
    table.add_column("Fecha Final", style="dim", width=12)
    table.add_column("Coincidencia [%]",justify="right")
    table.add_column("Diferencia [%]", justify="right")
    return table

@app.command()
def compare(lista:Path,
            panga:str="./SELECCION_PPP/panga",
            bnc:str="./SELECCION_PPP/bnc",
            destino:Path="./resultados", proc:int=6):
    console = Console()
    click.echo(lista)
    tabla = crear_tabla()
    stations =  read_file(lista)
    pprint(stations)
    fieldnames = "datetime,X,ErrX,Y,ErrY,Z,ErrZ,N,ErrN,E,ErrE,U,ErrU".split(",")
    for sta in stations:
        typer.echo(f"Listing {sta}")
        data_panga = ls(panga,f"*{sta}*_PPP/*", read_json, proc=proc)
        msg = typer.style(f"Panga {sta}, {len(data_panga)}",
                          fg=typer.colors.GREEN, bold=True)
        typer.echo(msg)
        partial_read_csv=partial(read_csv, fieldnames=fieldnames, ext="ppp")
        data_bnc = ls(bnc,f"*{sta}*.ppp.csv", partial_read_csv, proc=proc)
        msg = typer.style(f"BNC {sta} {len(data_bnc)}", fg=typer.colors.RED, bold=True)
        typer.echo(msg)
        procesar_data(sta, data_bnc, data_panga, tabla, console)
    console.print(tabla)

    


    

if __name__ == "__main__":
    app()
