from typing import List, Tuple, Set
from datetime import datetime

Dataset=List[Tuple[datetime, dict]]
DataKeys=Set[datetime]
