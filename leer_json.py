from pathlib import Path
import ujson as json
from pprint import pprint
from dateutil import parser
from datetime import datetime, timedelta
import sys
from leer_csv import read_csv
from dtypes import Dataset, DataKeys


def read_json(filepath:str)->Dataset:
    data = object
    with open(filepath) as fileread:
        data = json.load(fileread)
    return [(parser.parse(elem.get("DT_GEN")), elem) for elem in data.get('dataset', [])]


def leer_archivos(pathname, ext='json'):
    data = []
    path = Path(pathname)
    readers = {
        'json':read_json,
        "csv": read_csv
    }
    if path.exists():
        for pathfile in path.glob(f'*.*{ext}'):
            this_data = readers.get(ext,read_json)(pathfile)
            data += this_data
    return data


def delta(index, elem, lista, control=1.5):
    diff = parser.parse(elem.get('DT_GEN')) - parser.parse(lista[index].get('DT_GEN'))
    if diff.total_seconds()>control:
        return index+1, diff
    else:
        return index+1, 0

if __name__ == "__main__":    
    pathname = "".join(sys.argv[1])
    print(f"Analizando {pathname}")
    datos_json = leer_archivos(pathname)
    cantidad = len(datos_json)
    print("Cantidad de datos: ",cantidad)
    print("Efectividad al día, frecuencia 1s: ", cantidad/(24*3600))
    control=3
    print(f"Puntos con sobre {control} segundos")
    long = tuple(
        filter(lambda e:(e[1].total_seconds() if isinstance(e[1],
                                                            timedelta)
                         else e[1])>0,
               map(lambda par: delta(*par,datos_json,control),
                   enumerate(datos_json[1:])
               )
        )
    )
    print(f"Cantidad de puntos {len(long)}")
