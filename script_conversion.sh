function conversion () {
echo $@ 
lista=$(ls "$@"*.ppp)
for item in ${lista[@]}
do
    filename=$(echo $item|awk -F'/' '{print $NF}')
    filepath=$(echo $item|awk -F'/' 'BEGIN{OFS="/"}{--NF; print $0}')
    echo "Procesando $filename..."
    echo "En path: $filepath"
    awk 'BEGIN{
    OFS=",";
    print "datetime","X","ErrX","Y","ErrY","Z","ErrZ","N","ErrN","E","ErrE","U","ErrU"}
    {print $1,$5,$7,$10,$12,$15,$17,$20,$22,$25,$27,$30,$32}' $filepath/$filename > $filepath/$filename".csv"
    echo "$filename.csv listo!"
    #python3 read_csv.py
done
}


# Script::

path="SELECCION_PPP/bnc/"
conversion $path
