#// read json path
from leer_json  import leer_archivos
#// read csv
from leer_csv import read_csv, list_to_dict
#// handle input args
from pprint import pprint
import sys
import matplotlib.pyplot as plt


if __name__ == "__main__":
    pathname_json = sys.argv[1]
    pathname_csv = sys.argv[2]
    json_data = leer_archivos(pathname_json)
    panga_data = list_to_dict(json_data,main_key='DT_GEN')
    csv_data = read_csv(pathname_csv)
    pprint(sorted(list(csv_data.keys()))[0:10])
    pprint(sorted(list(panga_data.keys()))[0:10])
    keys_unicas = csv_data.keys() - panga_data.keys()
    data_no_panga =  {key:csv_data.get(key) for key in keys_unicas}
    list_no_panga = sorted(data_no_panga.keys())
    fechas_ordenadas = sorted(csv_data.keys())
    fechas_panga = sorted(panga_data.keys())
    all_data_binary = [(v, 1 if v in list_no_panga else None) for v in fechas_ordenadas]
    print("Data que no está en panga", len(data_no_panga))
    #pprint(all_data_binary)
    #pprint(data_no_panga)
    from_csv = [(v, float(csv_data.get(v).get("E"))) for v in fechas_ordenadas]
    #for v in fechas_panga:
    #    print(panga_data.get(v)["features"][0]["geometry"]["coordinates"])
    from_panga = [(v, panga_data.get(v)["features"][0]["geometry"]["coordinates"][0]) for v in fechas_panga]
    pprint(from_csv[:10])
    pprint(from_panga[:10])
    fig, axs = plt.subplots(2,1)
    l1,l2=axs[0].plot(*zip(*from_csv),*zip(*from_panga))
    axs[0].legend((l1,l2),("PPP","PANGA"),loc="upper right",shadow=True)
    axs[0].set_xlabel("time")
    axs[0].set_ylabel("E[m]")
    axs[1].scatter(*zip(*all_data_binary))
    axs[1].set_xlabel("time")
    axs[1].set_ylabel("1 = no está en panga")
    axs[0].grid(True)
    axs[1].grid(True)
    fig.tight_layout()
    plt.savefig("cons_ppp_panga.svg")
    plt.show()
